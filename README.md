# Configuración inicial del proyecto
Comenzamos creando el proyecto y definiendo las dependencias que necesitemos:

``` JavaScript
ng new angular-form-array
```

``` JavaScript
ng add @angular/material
```
En el módulo principal de la aplicación (AppModule), importamos el módulo que nos permitirá definir el formulario dinámicamente, así como un par de módulos de Angular Material que utilizaremos al definir el formulario en la plantilla:


``` JavaScript
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
 
import { AppComponent } from './app.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatInputModule, MatButtonModule } from '@angular/material';
 
@NgModule({
declarations: [
  AppComponent
],
imports: [
  BrowserModule,
  NoopAnimationsModule,
  ReactiveFormsModule,
  MatInputModule,
  MatButtonModule
],
providers: [],
bootstrap: [AppComponent]
})
export class AppModule { }
```
Una vez configurado el proyecto, pasamos a definir el formulario en el componente.

# Creando el formulario
Vamos a editar el componente AppComponent, donde definiremos el formulario dinámico:

``` JavaScript
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';

@Component({
selector: 'app-root',
templateUrl: './app.component.html',
styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

form: FormGroup;

ngOnInit() {
  this.form = new FormGroup({
    'nombre': new FormControl('', Validators.required),
    'email': new FormControl('', [Validators.required, Validators.email]),
    'direcciones': new FormArray([])
  });
}

annadirDireccion() {
  (<FormArray>this.form.controls['direcciones']).push(new FormGroup({
    'direccion': new FormControl('', Validators.required),
    'telefono': new FormControl('', Validators.required)
  }));
}

eliminarDireccion(index: number) {
  (<FormArray>this.form.controls['direcciones']).removeAt(index);
}

submit() {
  if (this.form.valid) {
    console.log(this.form.value);
  }
}

}
```

En el componente simplemente definimos una variable, que contendrá nuestro formulario. Éste lo definiremos en el evento OnInit del componente.

Además de crear el formulario, tenemos dos funciones que utilizaremos cuando queramos añadir o eliminar una dirección del formulario. En ambos casos, tratamos la propiedad “direcciones” del formulario como un array: si queremos añadir una nueva dirección, utilizamos el método “push”; si queremos eliminar una dirección, utilizamos el método “removeAt”, al pasamos como parámetro el índice que ocupa el bloque a eliminar.

Cada dirección está compuesta por dos propiedades, dirección y teléfono. Cada una de ellas la definimos como un FormControl. El conjunto de estas dos propiedades se corresponde con un FormGroup. Y el conjunto de direcciones se correspondería con el FormArray.

Además de las direcciones, definimos dos campos más “nombre” y “email”, que en este caso son estáticos y no se verán modificamos. Aprovechamos también esta forma de definir el formulario para indicar en cada campo la validación que necesitamos.

# Definiendo la plantilla para el formulario

Editamos la plantilla correspondiente al componente para definir la estructura del formulario:

``` html
<div style="text-align:center">
<h1>
  Formularios dinámicos en Angular usando FormArray
</h1>
</div>
<form [formGroup]="form" (submit)="submit()" style="text-align:center">
<div>
  <mat-form-field>
    <input matInput placeholder="Nombre" formControlName="nombre">
    <mat-error *ngIf="form.controls['nombre'].touched && form.controls['nombre'].hasError('required')">El nombre es obligatorio</mat-error>
  </mat-form-field>
</div>
<div>
  <mat-form-field>
    <input matInput placeholder="Email" formControlName="email">
    <mat-error *ngIf="form.controls['email'].touched && form.controls['email'].hasError('required')">El email es obligatorio</mat-error>
    <mat-error *ngIf="form.controls['email'].touched && form.controls['email'].hasError('email')">El email no es válido</mat-error>
  </mat-form-field>
</div>
<div class="direcciones" formArrayName="direcciones">
  <h3>Direcciones</h3>
  <div class="direccion" [formGroupName]="i" *ngFor="let direccion of form.controls['direcciones'].controls; let i = index">
    <div>
      <mat-form-field>
        <input matInput placeholder="Dirección" formControlName="direccion">
        <mat-error *ngIf="form.controls['direcciones'].controls[i].controls['direccion'].touched && form.controls['direcciones'].controls[i].controls['direccion'].hasError('required')">La dirección es obligatoria</mat-error>
      </mat-form-field>
    </div>
   
    <div>
      <mat-form-field>
        <input matInput placeholder="Teléfono" formControlName="telefono">
        <mat-error *ngIf="form.controls['direcciones'].controls[i].controls['telefono'].touched && form.controls['direcciones'].controls[i].controls['telefono'].hasError('required')">El teléfono es obligatorio</mat-error>
      </mat-form-field>
    </div>
     
    <button mat-raised-button color="warn" type="button" (click)="eliminarDireccion(i)">Eliminar dirección</button>
     
  </div>

  <button mat-raised-button color="primary" type="button" (click)="annadirDireccion()">Añadir dirección</button>
</div>

<button mat-raised-button color="accent">Enviar</button>

</form>
```

Como podemos ver, todo nuestro formulario está contenido en un nodo “form”, donde definimos que el formulario mediante la propiedad “formGroup”.

A continuación, representamos los campos estáticos “nombre” y “email”, definiendo en cada caso los mensajes de error, según el caso.

Por último, pasamos a definir nuestro bloque de direcciones. En el div que las contiene, definimos el nodo del formulario que contiene el formArray mediante la propiedad formArrayName.

Seguimos definiendo los bloques que se repetirán por cada dirección que hayamos añadido. Para ello, utilizamos la directiva estructural ngFor, iterando sobre la propiedad controls de nuestro formArray. En este mismo bloque que iteramos, definimos nombre de cada grupo que se representa mediante la propiedad formGroupName. Lo más común es utilizar el índice de la iteración por la que vamos.

Finalmente, dentro de cada grupo que iteramos definimos los campos de cada dirección, con sus correspondientes validaciones.

Si pulsáis en el botón “enviar”, podréis ver en consola el valor del formulario. Al definir éste de forma dinámica y utilizando FormArray, podremos obtener como resultado un objeto JSON igual o muy similar al que necesitaríamos, por ejemplo, para comunicarnos con una API externa.

El resultado visual del formulario sería el siguiente:

![](./imagenes/Selección_186.png)


<a href="https://www.facebook.com/oscar.sambache" target="_blank"><img alt="Encuentrame en facebook:" height="35" width="35" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTANMlMcb_-pUSuzjnpvSynOA3Ontg9Z2I1NE24WQ_KAk34yYmh"/> Oscar Sambache</a>